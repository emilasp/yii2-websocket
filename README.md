Модуль WebSocket для Yii2
=============================

Модуль для обмена сообщениями между клиентом и сервером

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-websocket": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-websocket.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем в console config:

```php
'controllerMap'       => [
        'websocket' => 'emilasp\websocket\console\controllers\WebsocketController',
    ],
```

