<?php
namespace emilasp\websocket\common\components;

use yii\base\Component;
use yii\helpers\Html;

/**
 * Class WsConnect
 * @package emilasp\websocket\common\components
 */
class WsConnect extends Component
{
    const KEY_WS = 'dfgv45hxk63edvnt';

    const MESSAGE_TYPE_NOTICE  = 'notice';
    const MESSAGE_TYPE_CONSOLE = 'console';
    const MESSAGE_TYPE_HANDLER = 'handler';

    public $servers;

    protected $_instances = [];

    public function log($message, $replace = false)
    {
        $file = \Yii::getAlias('@frontend/runtime/websocket.log');

        if ($replace) {
            file_put_contents($file, $message);
        } else {
            file_put_contents($file, $message . "\n", FILE_APPEND);
        }
    }

    public function getInstance($server)
    {
        if (!isset($this->_instances[$server])) {
            $this->_instances[$server] = stream_socket_client(
                $this->servers[$server]['localsocket'],
                $errno,
                $errstr
            );//соединямся с мастер-процессом:
        }
        return $this->_instances[$server];
    }

    public function send($message, $server = null)
    {
        if (!$server) {
            reset($this->servers);
            $server = key($this->servers);
        }
        return fwrite($this->getInstance($server), $message . "\n");
    }

    /** Отправляем сообщение пользователю
     *
     * @param integer $userId
     * @param string $type
     * @param string $message
     * @param string $params
     * @param string $server
     *
     * @return int
     */
    public function sendByUserId($userId, $type, $message, $params = [], $server = 'websocket')
    {
        $message = json_encode([
            'user_id' => $userId,
            'message' => $this->compileMessage($message, $type, $params),
        ]);

        if (!$server) {
            reset($this->servers);
            $server = key($this->servers);
        }
        return fwrite($this->getInstance($server), $message . "\n");
    }

    /** Собираем сообщение
     *
     * @param string $message
     * @param string $type
     * @param array $params
     *
     * @return array|string
     */
    public function compileMessage($message, $type = WsConnect::MESSAGE_TYPE_CONSOLE, $params = [])
    {
        if (!isset($params['from'])) {
            $params['from'] = 'system';
        }
        $data = [
            'type'    => $type,
            'message' => $message,
            'params'  => $params,
        ];
        return json_encode($data);
    }
}
