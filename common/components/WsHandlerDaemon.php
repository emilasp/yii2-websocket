<?php
namespace emilasp\websocket\common\components;

use emilasp\core\helpers\CryptHelper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/** Обрабатываем события воркера
 * Class WsHandlerDaemon
 * @package emilasp\websocket\common\components
 */
class WsHandlerDaemon extends \morozovsk\websocket\Daemon
{
    public $userIds = [];

    /** Вызывается при соединении с новым клиентом
     *
     * @param $connectionId
     * @param $info
     */
    protected function onOpen($connectionId, $info)
    {
        $message = 'пользователь #' . $connectionId . ' : '
                   . stream_socket_get_name($this->clients[$connectionId], true);

        $this->setDataUser($info, $connectionId);

        $data = Yii::$app->websocket->compileMessage($message . ' ' . $this->getUserId($connectionId));

        foreach ($this->clients as $clientId => $client) {
            $this->sendToClient($clientId, $data);
        }
    }

    /** Вызывается при закрытии соединения с существующим клиентом
     *
     * @param $connectionId
     */
    protected function onClose($connectionId)
    {
        unset($this->userIds[$connectionId]);
    }

    /** Вызывается при получении сообщения от клиента
     *
     * @param $connectionId
     * @param $newMessage
     * @param $type
     */
    protected function onMessage($connectionId, $newMessage, $type)
    {
        if (!strlen($newMessage)) {
            return;
        }

        //шлем всем сообщение, о том, что пишет один из клиентов
        $message = '#' . $connectionId . ' : ' . strip_tags($newMessage) . '-' . $this->getUserId($connectionId);


        $dataFromClient = json_decode($newMessage, true);

        if ($dataFromClient && isset($dataFromClient['action'])) {
            if ($dataFromClient['action'] === 'handler') {
                $class = $dataFromClient['data']['class'];
                $method = $dataFromClient['data']['method'];

                $obj = new $class();
                
                if (is_object($obj) && method_exists($obj, $method)) {
                    $message = $obj->{$method}($dataFromClient['data']);
                }
                
                
            }
        }
        

        $data =  Yii::$app->websocket->compileMessage($message, WsConnect::MESSAGE_TYPE_HANDLER, $message);

        $this->sendToClient($connectionId, $data);
        
        /*foreach ($this->clients as $clientId => $client) {
            $this->sendToClient($clientId, $data);
        }*/
    }

    /**
     * @param $connectionId
     * @param $data
     */
    protected function onServiceMessage($connectionId, $data)
    {
        $data = json_decode($data);

        foreach ($this->userIds as $clientId => $userId) {
            if ($data->user_id == $userId) {
                $this->sendToClient($clientId, $data->message);
            }
        }

        /*if (isset($this->clients[$data->clientId])) {
            $this->sendToClient($data->clientId, $data->message);
        }*/
    }




    /** Получаем ID клиента
     *
     * @param $connectionId
     *
     * @return mixed
     */
    private function getUserId($connectionId)
    {
        return $this->userIds[$connectionId];
    }

    /** Устанавливаем ID клиента
     *
     * @param $info
     * @param $connectionId
     */
    private function setDataUser($info, $connectionId)
    {
        $info['GET'];//or use $info['Cookie'] for use PHPSESSID or $info['X-Real-IP'] if you use proxy-server like nginx
        parse_str(substr($info['GET'], 1), $_GET);//parse get-query

        Yii::$app->websocket->log(VarDumper::dumpAsString($info));
        $this->userIds[$connectionId] = CryptHelper::decode($_GET['identity'], WsConnect::KEY_WS);
    }
}