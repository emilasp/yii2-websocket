Websocket = function () {
    var obj = this;
    var timeout = 1;

    var identity
    var url;
    var blockWsClient;
    var blockWsConsole;
    var blockWsConsoleDisplay;
    var sendInput;

    var loading = false;

    this.connect = function () {
        if (!obj.loading) {
            obj.inConsole('Пожалуйста подождите, идёт соединение с сервером.');
            obj.loading = true;
        }

        obj.ws = new WebSocket(obj.url + 'identity=' + obj.identity);

        obj.ws.onopen = function () {
            obj.loading = false;
            obj.onOpen();
        };
        obj.ws.onclose = function () {
            obj.loading = false;
            obj.onClose();
        };
        obj.ws.onmessage = function (evt) {
            obj.onMessage(evt);
        };
    }

    this.init = function () {
        obj.blockWsClient = $('#ws-client');
        obj.blockWsConsole = obj.blockWsClient.find('.debug-console');
        obj.blockWsConsoleDisplay = obj.blockWsConsole.find('#console');
        obj.identity = obj.blockWsClient.data('identity');
        obj.url = obj.blockWsClient.data('url');
        obj.sendInput = obj.blockWsClient.find('#sendInput');
        obj.sendInput.focus();

        obj.sendInput.on('keypress', function (e) {
            if (e.which == 13) {
                obj.ws.send(obj.sendInput.val());
                obj.sendInput.val('');
            }
        });

        obj.connect();
    }

    this.send = function (action, data) {
        var message = JSON.stringify({"action":action, "data":data});
        obj.ws.send(message);
    }

    this.onOpen = function () {
        obj.inConsole('Соединение открыто');
    }

    this.onClose = function () {
        if (!obj.loading) {
            obj.inConsole('Cоединение закрыто, пытаюсь переподключиться..');
            obj.loading = true;
        }
        setTimeout(obj.connect(), obj.timeout);

    }

    this.onMessage = function (evt) {
        var data = JSON.parse(evt.data);
        var type = data.type;

        switch (type) {
            case 'handler':
                console.log(data);
                obj.inHandler(data.message, data.params);
                break;
            case 'console':
                obj.inConsole(data.message, data.params);
                break;
            case 'notice':
                obj.notice(data.message, data.params);
                break;
            case 'reload':
                obj.reload(data.message, data.params);
                break;
        }
    }


    this.inConsole = function (message, params) {
        var defaultFrom = 'System';
        var defaultFromClass = 'orange';
        var defaultTextClass = '';
        var fromClass = '';

        if (typeof params === 'undefined') {
            params = {
                from: defaultFrom,
                "class": defaultTextClass
            };
            fromClass = defaultFromClass;
        }

        if (typeof params.from === 'undefined') {
            params.from = 'System';
            fromClass = defaultFromClass;
        }

        params.class = typeof params.class !== 'undefined' ? params.class : defaultTextClass;


        var rowHtml = '<div class="console-row">'
                    + '<strong class="console-name ' + fromClass + '">' + params.from + ':</strong>'
                    + '<span class="console-text ' + params.class + '">' + message + '</span>'
                    + '</div>';

        obj.blockWsConsoleDisplay.append(rowHtml);
        obj.blockWsConsoleDisplay.scrollTop(obj.blockWsConsoleDisplay.prop('scrollHeight'));
    }
    this.notice = function (message, params) {
        notice(message, params['color']);
    }
    this.reload = function (selector, params) {
        if ($('#' + selector).length) {
            $.pjax.reload({
                container: '#' + selector,
                timeout: 0,
                push: false,
                //url: "/strategic/task/update-ajax",
                "data": params
            });
        }
    }
    this.inHandler = function (objectName, params) {
        window[params['class']][params['method']](params['data'])
    }
}

var websocket;
$(function () {
    websocket = new Websocket();
    websocket.init();
});