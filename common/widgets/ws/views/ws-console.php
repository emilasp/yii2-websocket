<?php
use yii\helpers\Url;

?>
<div class="debug-console <?= $positionRight ? 'console-right' : '' ?>">
    <div class="title">Консоль
        <small><?= date('d.m.Y') ?></small>
    </div>
    <div id="console"></div>
    <div class="navbar-bottom">
        <input id="sendInput" type="text" class="form-control" placeholder="Text input" autocomplete="off">
    </div>
</div>