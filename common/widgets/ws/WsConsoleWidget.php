<?php

namespace emilasp\websocket\common\widgets\ws;

use emilasp\core\components\base\Widget;
use emilasp\core\helpers\CryptHelper;
use emilasp\websocket\common\components\WsConnect;
use yii;
use yii\helpers\Url;

/**
 * Class WsConsoleWidget
 * @package emilasp\websocket\common\widgets\ws
 */
class WsConsoleWidget extends Widget
{
    public $positionRight = false;
    
    private $identity;
    private $consoleHtml = '';

    public function init()
    {
        //Yii::$app->websocket->sendByUserId(1, 'reload', 'dashGoalGridCur');

        $this->debug    = YII_DEBUG;
        $this->identity = CryptHelper::encode(Yii::$app->user->id, WsConnect::KEY_WS);

        if ($this->debug) {
            $this->consoleHtml = $this->render('ws-console', [
                'positionRight' => $this->positionRight
            ]);
        }
    }

    public function run()
    {
        if (!Yii::$app->user->isGuest) {
            $this->registerAssets();

            echo $this->render('ws-client', [
                'url'      => Url::toRoute('/ws/', 'ws'),
                'identity' => $this->identity,
                'console'  => $this->consoleHtml,
            ]);
        }
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        WsAsset::register($view);
    }
}
