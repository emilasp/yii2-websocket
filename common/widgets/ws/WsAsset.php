<?php

namespace emilasp\websocket\common\widgets\ws;

use emilasp\core\components\base\AssetBundle;

/**
 * Class WsAsset
 * @package emilasp\websocket\common\widgets\ws
 */
class WsAsset extends AssetBundle
{
    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $js = ['ws'];
    public $css = ['ws'];
    public $sourcePath = __DIR__ . '/assets';
}
